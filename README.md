# The ASM Assembly #

Welcome to the ASM Assembly low-level coding club.

Basic goals:

* lowering the barrier to entry for assembly programming
* sharing work and learning resources related to assembly and low-level programming in general
* writing our own assemblers
* simplifying assembly-language documentation
* ... and building cool stuff from here
